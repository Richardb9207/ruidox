import { Component, OnInit } from '@angular/core';
import { DBMeter } from '@ionic-native/db-meter/ngx';
import { HTTP } from '@ionic-native/http/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{
  
  requestObject: any = null;
  db  = 0;
  lat : any;
  lon :  any;
  argunmento = null;

  constructor(public dbMeter: DBMeter, 
              private http:HTTP, 
              private geolocation: Geolocation,
              public activatedRoute: ActivatedRoute
              ) { 
  }

  ngOnInit(){
    this.argunmento = this.activatedRoute.snapshot.paramMap.get('id');
  }

  switchDb(){
    this.dbMeter.start().subscribe(
      data =>{
        this.db=Number(data);
      }
    );
    console.log(this.db);
  }

  postRequest(){
    this.http.post('https://ruidox-demo.cfapps.io/measurement/', 
    {"_id":"",
    "latitude":this.lat,
    "longitude":this.lon,
    "measurement":this.db,
    "userid":this.argunmento,
    "measurementDate":"",
    "device":{
    "_id":"5d052ccff2422f38e89f43c1",
    "operativeSystem":"",
    "osVersion":"",
    "brand":"",
    "model":"",
    "macaddres":""}}, {})
    .then(res => this.requestObject = res.data)
    .catch(err => this.requestObject = err)
    this.db=0;
    this.lat=0;
    this.lon=0;
  }

  getGeolocation(){
    this.geolocation.getCurrentPosition().then((resp) => {
      this.lat=resp.coords.latitude;
      this.lon=resp.coords.longitude;
     }).catch((error) => {
       console.log('Error getting location', error);
     });
  }
}

