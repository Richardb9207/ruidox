import { Injectable } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/auth";
import { promise } from 'protractor';
import { resolve } from 'dns';
import { reject } from 'q';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private AFauth:AngularFireAuth) { }

  login(usuario:string, password:string){
    return new Promise((resolve,reject)=>{
    this.AFauth.auth.signInWithEmailAndPassword(usuario,password).then(usuario =>{
      resolve(usuario)
    }).catch(err => reject(err));
    })
  }
}
