import { Component, OnInit } from '@angular/core';
import { AuthService } from "../../servicios/auth.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  usuario : string;
  password :string;

  constructor(private authService :AuthService, 
    public router:Router) { }


  ngOnInit() {
    
  }

  OnSubmitLogin(){
    this.authService.login(this.usuario, this.password).then(res=>{
        this.router.navigate(['/home',this.password]);
    }).catch(err => alert('Datos Incorrectos'));
  }
}
