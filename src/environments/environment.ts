// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const firebaseConfig = {
  apiKey: "AIzaSyCA2rB5kbSQh3mRHxEXXGCeBwp3sLRYOMA",
  authDomain: "ruidox-795d6.firebaseapp.com",
  databaseURL: "https://ruidox-795d6.firebaseio.com",
  projectId: "ruidox-795d6",
  storageBucket: "ruidox-795d6.appspot.com",
  messagingSenderId: "1081471972544",
  appId: "1:1081471972544:web:8383f04a7b041d1d"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
